//
//  ViewController.swift
//  Apple Pay
//
//  Created by Akash Sidhwani on 21/12/21.
//

import UIKit
import PassKit

class ViewController: UIViewController {
    
    var animate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        createPaymentRequest(12)
    }
    // MARK: Create payment request
    func createPaymentRequest(_ amount: Int) {
        // create item with name and price
        let paymentItem = PKPaymentSummaryItem()
        
        paymentItem.label = "Name of the Product"
        paymentItem.amount = NSDecimalNumber(value: amount)
        
        // payment networks holds allowed cards
        let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
        
        // Will check if the user can make payment
        // throws an error if for any case it is not allowed
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
            
            // creates a request with required properties
            // check https://developer.apple.com/documentation/passkit/pkpaymentrequest
            // for other available properties
            let request = PKPaymentRequest()
            request.currencyCode = "USD" // currency code
            request.countryCode = "US" // country code
            request.merchantIdentifier = "merchant.com.successive.applePay" // add Merchant id here
            request.merchantCapabilities = PKMerchantCapability.capability3DS // payment security
            request.supportedNetworks = paymentNetworks // cards that are allowed
            request.paymentSummaryItems = [paymentItem] // The items that needs to be billed
            
            let address : Set<PKContactField> = Set(arrayLiteral: PKContactField.name, PKContactField.postalAddress)
            
            request.requiredShippingContactFields = address
            
//            let shippingAddress = PKContact()
//            shippingAddress.name = PersonNameComponents(namePrefix: "", givenName: "Akash", middleName: "", familyName: "Sidhwani", nameSuffix: "", nickname: "", phoneticRepresentation: nil)
//            let postalAddress = CNPostalAddress()
//            postalAddress.stree = "Sector 51"
//            postalAddress.postalCode = "160051"
//            postalAddress.state = "Chandigarh"
//
//            shippingAddress.postalAddress = postalAddress
            
            // pass the request to redirect it to Payment View Controller
            goToPaymentViewController(request)
        } else {
            displayDefaultAlert(title: "Error", message: "Unable to make Apple Pay transaction.")
        }
    }
    
    func goToPaymentViewController(_ request: PKPaymentRequest) {
        // native payment view controller, passing the payment request in it
        guard let paymentVC = PKPaymentAuthorizationViewController(paymentRequest: request) else {
            displayDefaultAlert(title: "Error", message: "Unable to present Apple Pay authorization.")
            return
        }
//        paymentAuthorizationViewController?(paymentVC, didSelect: <#T##PKShippingMethod#>, handler: { updatedRequest in
//
//        })
        
        paymentVC.delegate = self
        self.present(paymentVC, animated: false, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.dismiss(animated: false, completion: nil)
//            self.createPaymentRequest(20)
        }
    }
    
    func displayDefaultAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
// MARK: Payment Delegate Methods
extension ViewController: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        dismiss(animated: true, completion: nil)
        displayDefaultAlert(title: "Success!", message: "The Apple Pay transaction was complete.")
    }
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact) async -> PKPaymentRequestShippingContactUpdate {
//        print(contact)
//        return PKPaymentRequestShippingContactUpdate(paymentSummaryItems: [PKPaymentSummaryItem.init(label: "Name of the Profuct", amount: NSDecimalNumber(value: 12))])
//    }
}
struct Address {
  var Street: String?
  var City: String?
  var State: String?
  var Zip: String?
  var FirstName: String?
  var LastName: String?

  init() {
  }
}
